﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.WebStore.Custom.ViewModel
{
    public class RSCategoryViewModel
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }

        public string SEOUrl { get; set; }
        public int[] ProductIds { get; set; }
        public string ParentCategoryName { get; set; }
        public string ParentSEODetails { get; set; }

        public int ParentCategoryId { get; set; }
        public int ParentProductIdCount { get; set; }

        public int ProductIdCount { get; set; }

    }
}
