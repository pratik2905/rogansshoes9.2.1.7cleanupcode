﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.WebStore;
using Znode.WebStore.Custom.Agents.Agents;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.WebStore.Custom.Helper
{
    public class RSWebstoreHelper : BaseAgent
    {
        public IRSWidgetDataAgent RSWidgetDataAgent()
          => new RSWidgetDataAgent(GetClient<WebStoreWidgetClient>(), GetClient<PublishProductClient>(), GetClient<BlogNewsClient>());

    }
}
