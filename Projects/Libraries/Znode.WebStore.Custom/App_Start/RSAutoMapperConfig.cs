﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Sample.Api.Model.RSCustomizedFormModel;
using Znode.WebStore.Custom.ViewModel;

namespace Znode.WebStore.Custom.App_Start
{
    public static class RSAutoMapperConfig
    {
        public static void Execute()
        {
           
            Mapper.CreateMap<PublishCategoryModel, RSCategoryViewModel>()
                .ForMember(d => d.CategoryId, opt => opt.MapFrom(src => src.PublishCategoryId))
                .ForMember(d => d.CategoryName, opt => opt.MapFrom(src => src.Name))
                .ForMember(d => d.ProductIdCount, opt => opt.MapFrom(src => src.ProductIds.Count()))
                .ForMember(d => d.SEOUrl, Opt => Opt.MapFrom(src => src.SEODetails.SEOUrl));
            Mapper.CreateMap<ScheduleATruckModel, ScheduleATruckViewModel>().ReverseMap();
            Mapper.CreateMap<ShoesFinderModel, ShoesFinderViewModel>().ReverseMap();
           

        }
    }
}
