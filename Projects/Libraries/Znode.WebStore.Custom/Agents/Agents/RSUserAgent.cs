﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.Maps;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Resources;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSUserAgent : UserAgent, IUserAgent, IRSUserAgent
    {
        private readonly IUserClient _userClient;
        public RSUserAgent(ICountryClient countryClient, IWebStoreUserClient webStoreAccountClient, IWishListClient wishListClient, IUserClient userClient, IPublishProductClient productClient, ICustomerReviewClient customerReviewClient, IOrderClient orderClient,
          IGiftCardClient giftCardClient, IAccountClient accountClient, IAccountQuoteClient accountQuoteClient, IOrderStateClient orderStateClient, IPortalCountryClient portalCountryClient, IShippingClient shippingClient, IPaymentClient paymentClient, ICustomerClient customerClient, IStateClient stateClient, IPortalProfileClient portalProfileClient) :
            base(countryClient, webStoreAccountClient, wishListClient, userClient, productClient, customerReviewClient, orderClient, giftCardClient, accountClient, accountQuoteClient,
                orderStateClient, portalCountryClient, shippingClient, paymentClient, customerClient, stateClient, portalProfileClient)
        {
            _userClient = GetClient<IUserClient>(userClient);
        }

        public override LoginViewModel Login(LoginViewModel model)
        {
            LoginViewModel loginViewModel;
            try
            {
                model.PortalId = PortalAgent.CurrentPortal?.PortalId;
                //Authenticate the user credentials.
                UserModel userModel = _userClient.Login(UserViewModelMap.ToLoginModel(model), GetExpands());
                if (HelperUtility.IsNotNull(userModel))
                {
                    //Check of Reset password Condition.
                    if (HelperUtility.IsNotNull(userModel.User) && !string.IsNullOrEmpty(userModel.User.PasswordToken))
                    {
                        loginViewModel = UserViewModelMap.ToLoginViewModel(userModel);
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword, true, loginViewModel);
                    }
                    // Check user associated profiles.                
                    if (!userModel.Profiles.Any())
                    {
                        return ReturnErrorModel(WebStore_Resources.ProfileLoginFailedErrorMessage);
                    }

                    //Save the User Details in Session.
                    SaveInSession(WebStoreConstants.UserAccountKey, userModel.ToViewModel<UserViewModel>());
                    /*NIVI CODE START: TO SAVE DEFAULT STORE FOR USER*/

                    string defaultStore = GetFromCookie("DefaultSelectedStore");
                    string[] storeValues = defaultStore.Split('*');
                    if (storeValues.Length > 1)
                    {
                        userModel.Custom1 = storeValues[0];
                        userModel.Custom2 = storeValues[1];
                        userModel.Custom3 = storeValues[2];
                        this.UpdateProfile(userModel.ToViewModel<UserViewModel>(), true);
                        SaveInSession(WebStoreConstants.UserAccountKey, userModel.ToViewModel<UserViewModel>());
                    }
                    else
                    {
                        string defaultstorestring = userModel.Custom1 + "*" + userModel.Custom2 + "*" + userModel.Custom3;
                        SaveInCookie("DefaultSelectedStore", defaultstorestring);

                    }
                    HttpContext.Current.Session["FirstName"] = userModel.FirstName;
                    /*NIVI CODE END: TO SAVE DEFAULT STORE FOR USER*/
                    return UserViewModelMap.ToLoginViewModel(userModel);
                }
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                switch (ex.ErrorCode)
                {
                    case 2://Error Code to Reset the Password for the first time login.
                        return ReturnErrorModel(ex.ErrorMessage, true);
                    case ErrorCodes.AccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorAccountLocked);
                    case ErrorCodes.LoginFailed:
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
                    case ErrorCodes.TwoAttemptsToAccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorTwoAttemptsRemain);
                    case ErrorCodes.OneAttemptToAccountLocked:
                        return ReturnErrorModel(WebStore_Resources.ErrorOneAttemptRemain);
                    default:
                        return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
            }
            return ReturnErrorModel(WebStore_Resources.InvalidUserNamePassword);
        }

        private LoginViewModel ReturnErrorModel(string errorMessage, bool hasResetPassword = false, LoginViewModel model = null)
        {
            if (HelperUtility.IsNull(model))
            {
                model = new LoginViewModel();
            }

            //Set Model Properties.
            model.HasError = true;
            model.IsResetPassword = hasResetPassword;
            model.ErrorMessage = string.IsNullOrEmpty(errorMessage) ? WebStore_Resources.InvalidUserNamePassword : errorMessage;
            return model;
        }

        private ExpandCollection GetExpands()
        {
            return new ExpandCollection()
                {
                    ExpandKeys.Addresses,
                    ExpandKeys.Profiles,
                    ExpandKeys.WishLists,
                    ExpandKeys.Orders,
                    ExpandKeys.OrderLineItems,
                    ExpandKeys.GiftCardHistory,
                };
        }

        public override UserViewModel UpdateProfile(UserViewModel model, bool webStoreUser)
        {
            UserViewModel userViewModel = GetUserViewModelFromSession();

            if (HelperUtility.IsNotNull(userViewModel))
            {
                MapUserProfileData(model, userViewModel);
            }
            else
            {
                userViewModel = model;
            }

            try
            {
                _userClient.UpdateUserAccountData(userViewModel.ToModel<UserModel>(), webStoreUser);
                SaveInSession(WebStoreConstants.UserAccountKey, userViewModel);
                userViewModel.SuccessMessage = WebStore_Resources.SuccessProfileUpdated;
                Znode.Engine.WebStore.Helper.ClearCache($"UserAccountAddressList{userViewModel.UserId}");
            }
            catch (ZnodeException ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Warning);
                return (UserViewModel)GetViewModelWithErrorMessage(userViewModel, WebStore_Resources.ErrorUpdateProfile);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, string.Empty, TraceLevel.Error);
                return (UserViewModel)GetViewModelWithErrorMessage(userViewModel, WebStore_Resources.ErrorUpdateProfile);
            }
            return userViewModel;
        }

        private static void MapUserProfileData(UserViewModel model, UserViewModel userViewModel)
        {
            userViewModel.Email = model.Email;
            userViewModel.EmailOptIn = model.EmailOptIn;
            userViewModel.FirstName = model.FirstName;
            userViewModel.LastName = model.LastName;
            userViewModel.PhoneNumber = model.PhoneNumber;
            userViewModel.UserName = model.UserName;
            userViewModel.ExternalId = model.ExternalId;
            userViewModel.Custom1 = model.Custom1;
            userViewModel.Custom2 = model.Custom2;
            userViewModel.Custom3 = model.Custom3;
            userViewModel.Custom4 = model.Custom4;
            userViewModel.Custom5 = model.Custom5;
        }

        public string GetDefaultStoreDetailsFromCookie()
        {
            string defaultStore = GetFromCookie("DefaultSelectedStore");
            return defaultStore;
        }

        public void SetDefaultStoreDetailsInCookie()
        {
            string defaultStore = GetFromCookie("DefaultSelectedStore");

        }
    }
}
