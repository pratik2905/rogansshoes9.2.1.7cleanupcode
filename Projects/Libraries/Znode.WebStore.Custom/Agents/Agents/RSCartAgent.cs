﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.Core.ViewModels;
using Znode.Engine.WebStore;
using Znode.Engine.WebStore.Agents;
using Znode.Engine.WebStore.ViewModels;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.WebStore.Custom.Agents.IAgents;


namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSCartAgent : CartAgent, ICartAgent, IRSCartAgent
    {
        #region Private member
        private readonly IShoppingCartClient _shoppingCartsClient;
        private readonly IPublishProductClient _publishProductClient;
        private readonly IAccountQuoteClient _accountQuoteClient;
        private decimal thresholdShippingValue = Convert.ToDecimal(ConfigurationManager.AppSettings["ThresholdShippingValue"]);
        private const string FedEx_Ground = "FedEx Ground";

        #endregion

        #region Constructor
        public RSCartAgent(IShoppingCartClient shoppingCartsClient, IPublishProductClient publishProductClient, IAccountQuoteClient accountQuoteClient, IUserClient userClient) : base(shoppingCartsClient, publishProductClient, accountQuoteClient, userClient)
        {
            _shoppingCartsClient = GetClient<IShoppingCartClient>(shoppingCartsClient);
            _publishProductClient = GetClient<IPublishProductClient>(publishProductClient);
            _accountQuoteClient = GetClient<IAccountQuoteClient>(accountQuoteClient);
        }
        #endregion

        #region PickUp/Ship
        public override AddToCartViewModel AddToCartProduct(AddToCartViewModel cartItem)
        {
            try
            {

                ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);
                if (cartItem != null)
                {
                    /*PICK UP IN STORE Cookie section*/
                    string defaultStore, defaultSelectedStoreDetails = string.Empty;
                    string selectedStoreID = ((string[])cartItem.Custom1)[0];
                    string selectedStoreName = ((string[])cartItem.Custom2)[0];
                    string selectedStoreAddress = ((string[])cartItem.Custom3)[0];
                    if (selectedStoreID == string.Empty)/*SHIP Case*/
                    {
                        defaultStore = GetFromCookie("DefaultSelectedStore");
                        string[] storeValues = defaultStore.Split('*');
                        defaultSelectedStoreDetails = defaultStore;
                        if (storeValues.Length > 1)
                        {
                            cartItem.Custom2 = storeValues[1];
                        }

                        cartItem.Custom4 = "SHIP";
                    }
                    else/*PICK UP Case*/
                    {
                        defaultSelectedStoreDetails = selectedStoreID + "*" + selectedStoreName + "*" + selectedStoreAddress;
                        cartItem.Custom4 = "PICKUP";
                    }

                    cartItem.CookieMappingId = GetFromCookie(WebStoreConstants.CartCookieKey);

                    cartItem.Coupons = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey)?.Coupons?.Count > 0 ? GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey)?.Coupons : new List<CouponModel>();

                    //Create new cart.
                    cartItem = _shoppingCartsClient.AddToCartProduct(GetShoppingCartValues(cartItem)?.ToModel<AddToCartModel>()).ToViewModel<AddToCartViewModel>();

                    SaveInCookie(WebStoreConstants.CartCookieKey, cartItem.CookieMappingId.ToString());

                    SaveInCookie("DefaultSelectedStore", defaultSelectedStoreDetails);

                    cartItem?.ShoppingCartItems.Where(x => x.SKU == cartItem.SKU).Select(x => { x.ProductType = cartItem.ProductType; return x; })?.ToList();

                    cartItem.ShippingId = (GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey)?.ShippingId).GetValueOrDefault();


                    SaveInSession(WebStoreConstants.CartModelSessionKey, cartItem);
                    SaveInSession("ToRetainStorevaluesChangedInCart", shoppingCartModel);

                    return cartItem;
                }

                return null;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("RSCartAgent:-" + ex.Message, "RSCartAgent", TraceLevel.Error, ex);
                return base.AddToCartProduct(cartItem);
            }
        }

        public override CartViewModel GetCart(bool isCalCulateTaxAndShipping = true, bool isCalculateCart = true)
        {

            ShoppingCartModel shoppingCartModelorg = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);
            if (shoppingCartModelorg == null)
            {
                shoppingCartModelorg = GetFromSession<ShoppingCartModel>("ToRetainStorevaluesChangedInCart");
            }

            CartViewModel cart = base.GetCart(isCalCulateTaxAndShipping);

            ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);


            if (shoppingCartModel != null)
            {
                if (shoppingCartModelorg != null)
                {
                    foreach (ShoppingCartItemModel s in shoppingCartModel.ShoppingCartItems)
                    {
                        ShoppingCartItemModel modifieditem = shoppingCartModelorg?.ShoppingCartItems?.Where(x => x.ProductId == s.ProductId).FirstOrDefault();
                        if (modifieditem != null)
                        {
                            s.Custom1 = modifieditem.Custom1;
                            s.Custom2 = modifieditem.Custom2;
                            s.Custom3 = modifieditem.Custom3;
                            s.Custom4 = modifieditem.Custom4;
                        }
                    }
                }
                string defaultStore = GetFromCookie("DefaultSelectedStore");
                string[] storeValues = defaultStore.Split('*');
                /*IF  -if Default Store is selected then all cart items should show this stores details
                  ELSE-copy details from previously saved cart */
                if (storeValues.Length > 1)
                {
                    foreach (ShoppingCartItemModel s in shoppingCartModel.ShoppingCartItems)
                    {
                        /*if case - if any items in cart have Pick up in store option selected then store details will be updated to latest default selected store.
                         else case - only storename will be updated to latest default selected store name for Ship Case*/
                        int prdid = s.ProductId;
                        if (s.Custom1 != "")
                        {
                            cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom1 = storeValues[0]);
                            cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom2 = storeValues[1]);
                            cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom3 = storeValues[2]);
                        }
                        else
                        {
                            cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom1 = s.Custom1);
                            cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom2 = storeValues[1]);
                            cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom3 = s.Custom3);
                        }
                    }
                }
                else
                {

                    foreach (ShoppingCartItemModel s in shoppingCartModel.ShoppingCartItems)
                    {
                        cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom1 = s.Custom1);
                        cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom2 = s.Custom2);
                        cart?.ShoppingCartItems.Where(x => x.ProductId == s.ProductId.ToString()).ToList().ForEach(cc => cc.Custom3 = s.Custom3);
                    }

                }
            }
            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
            return cart;
        }

        public void UpdateDeliveryPreferenceToPickUp(string storeId, string storeName, string storeAddress, int productId = 0)
        {
            ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);

            string deliverytype = shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == productId)?.FirstOrDefault()?.Custom4;
            if (storeId == "" && storeName != "")
            {
                /*Case Ship to PickUp by clicking on Pick Up*/
                string defaultStore = GetFromCookie("DefaultSelectedStore");
                if (defaultStore == "")
                {
                    defaultStore = "3*Rogans Shoes -Test Store 2*Sadar,Nagpur,";
                }

                string[] storeValues = defaultStore.Split('*');
                if (storeValues.Length > 1)
                {
                    storeId = storeValues[0];
                    storeName = storeValues[1];
                    storeAddress = storeValues[2];
                    shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == productId).ToList().ForEach(cc => cc.Custom4 = "PICKUP");
                }
                else
                {

                }
            }
            else
            {
                /*Case Pick up already clicked then click on Change Store Or Ship to Pick up by clicking Change Store*/
                shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == productId).ToList().ForEach(cc => cc.Custom4 = "PICKUP");
                string defaultSelectedStoreDetails = storeId + "*" + storeName + "*" + storeAddress;
                SaveInCookie("DefaultSelectedStore", defaultSelectedStoreDetails);
            }

            if (shoppingCartModel != null)
            {
                shoppingCartModel?.ShoppingCartItems.Where(x => x.Custom4 == "PICKUP").ToList().ForEach(cc => cc.Custom1 = storeId);
                shoppingCartModel?.ShoppingCartItems.Where(x => x.Custom4 == "PICKUP").ToList().ForEach(cc => cc.Custom2 = storeName);
                shoppingCartModel?.ShoppingCartItems.Where(x => x.Custom4 == "PICKUP").ToList().ForEach(cc => cc.Custom3 = storeAddress);
                shoppingCartModel?.ShoppingCartItems.Where(x => x.Custom4 == "PICKUP").ToList().ForEach(cc => cc.Custom4 = "PICKUP");

                shoppingCartModel?.ShoppingCartItems.Where(x => x.Custom4 == "SHIP").ToList().ForEach(cc => cc.Custom2 = storeName);
            }
            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);
            SaveInSession("CartWithUpdatedDeliveryPreference", shoppingCartModel);


        }

        public void UpdateDeliveryPreferenceToShip(int productId)
        {
            ShoppingCartModel shoppingCartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey);
            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == productId).ToList().ForEach(cc => cc.Custom1 = "");
            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == productId).ToList().ForEach(cc => cc.Custom3 = "");
            shoppingCartModel?.ShoppingCartItems.Where(x => x.ProductId == productId).ToList().ForEach(cc => cc.Custom4 = "SHIP");
            SaveInSession(WebStoreConstants.CartModelSessionKey, shoppingCartModel);

        }
        #endregion

        public override bool MergeCart()
        {
            ShoppingCartModel cart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                           GetCartFromCookie();

            //GetCurrent user's Id.
            int userId = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey).UserId;

            if (HttpContext.Current.Request.Url.AbsolutePath.ToUpper() != "/CHECKOUT/INDEX"
               && cart?.UserId != 0
               && userId != cart?.UserId)
            {
                //Nullify edit mode cart
                cart = null;
            }
            //Get shopping cart by userId.
            _shoppingCartsClient.SetProfileIdExplicitly(Znode.Engine.WebStore.Helper.GetProfileId().GetValueOrDefault());
            ShoppingCartModel cartModel = _shoppingCartsClient.GetShoppingCart(new CartParameterModel
            {
                UserId = userId,
                PortalId = PortalAgent.CurrentPortal.PortalId,
                LocaleId = PortalAgent.LocaleId,
                PublishedCatalogId = GetCatalogId().GetValueOrDefault()
            });

            //Check if cart persistent.
            CheckCartPersistent(userId, cartModel, cart);

            //to set user profile Id in shopping cart 
            cartModel.ProfileId = Znode.Engine.WebStore.Helper.GetProfileId();

            bool status = false;
            //Update cart
            if (cart?.ShoppingCartItems?.Count > 0)
            {
                status = UpdateCart(ref cartModel);
            }

            //Save cart in session.
            if (status)
            {
                SaveInSession(WebStoreConstants.CartMerged, true);
            }

            cartModel.ShippingId = (cart?.ShippingId).GetValueOrDefault();
            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);
            RemoveCookie(WebStoreConstants.CartCookieKey);
            return status;
        }

        /// <summary>
        /// Merge Cart after login
        /// </summary>
        /// <returns></returns>
        public override bool MergeGuestUserCart()
        {
            bool status = false;
            ShoppingCartModel cart = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ??
                                     GetCartFromCookie();
            List<ShoppingCartItemModel> OrgItems = new List<ShoppingCartItemModel>();
            //Merge cart
            if (cart?.ShoppingCartItems?.Count() > 0)
            {
                List<string> deliverytype = cart?.ShoppingCartItems.Select(x => x.Custom4).Distinct().ToList();

                if (deliverytype.Contains("PICKUP"))
                {
                    OrgItems = cart?.ShoppingCartItems;
                }

                RemoveInSession(WebStoreConstants.CartModelSessionKey);
                status = _shoppingCartsClient.MergeGuestUsersCart(GetFiltersForMergeCart(cart.CookieMappingId, Convert.ToInt32(cart.ShoppingCartItems.FirstOrDefault()?.OmsSavedcartLineItemId)));
            }

            //Get shopping cart by userId.
            _shoppingCartsClient.SetProfileIdExplicitly(Znode.Engine.WebStore.Helper.GetProfileId().GetValueOrDefault());
            ShoppingCartModel cartModel = _shoppingCartsClient.GetShoppingCart(new CartParameterModel
            {
                UserId = GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey).UserId,
                PortalId = PortalAgent.CurrentPortal.PortalId,
                LocaleId = PortalAgent.LocaleId,
                PublishedCatalogId = GetCatalogId().GetValueOrDefault()
            });

            cartModel.ShippingId = (cart?.ShippingId).GetValueOrDefault();
            cartModel.Coupons = cart?.Coupons;
            if (OrgItems.Count > 0)
            {
                foreach (ShoppingCartItemModel s in cartModel.ShoppingCartItems)
                {
                    ShoppingCartItemModel modifieditem = OrgItems?.Where(x => x.ProductId == s.ProductId).FirstOrDefault();
                    if (modifieditem != null)
                    {
                        s.Custom1 = modifieditem.Custom1;
                        s.Custom2 = modifieditem.Custom2;
                        s.Custom3 = modifieditem.Custom3;
                        s.Custom4 = modifieditem.Custom4;
                    }
                }
            }
            SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);
            RemoveCookie(WebStoreConstants.CartCookieKey);

            return status;
        }

        private FilterCollection GetFiltersForMergeCart(string cookieMappingId, int omsSavedCartLineItemId)
        {
            FilterCollection filters = new FilterCollection();
            filters.Add("UserId", FilterOperators.Equals, GetFromSession<UserViewModel>(WebStoreConstants.UserAccountKey).UserId.ToString());
            filters.Add(ZnodePortalEnum.PortalId.ToString(), FilterOperators.Equals, PortalAgent.CurrentPortal.PortalId.ToString());
            filters.Add("CookieMappingId", FilterOperators.Equals, cookieMappingId);
            filters.Add("OmsSavedCartLineItemId", FilterOperators.Equals, omsSavedCartLineItemId.ToString());
            return filters;
        }



        public override CartViewModel CalculateShipping(int shippingOptionId, int shippingAddressId, string shippingCode, string additionalInstruction = "")
        {

            /*On Load Case*/
            if (shippingCode == null || shippingCode.Contains("undefined"))
            {
                CartViewModel cartViewModel = base.CalculateShipping(shippingOptionId, shippingAddressId, shippingCode, additionalInstruction);
                if (cartViewModel.ShippingCost == 0 && cartViewModel.Shipping.ShippingDiscount != 0)
                {
                    cartViewModel.Shipping.ShippingDiscount = 0;
                }

                return cartViewModel;
            }
            /*On Click Case*/
            else
            {
                string[] shippingCodeval = shippingCode.Split('*');
                if (shippingCodeval.Length > 1)
                {
                    shippingCode = shippingCodeval[1];
                }
                else
                {
                    shippingCode = shippingCodeval[0];
                }

                CartViewModel cartViewModel = base.CalculateShipping(shippingOptionId, shippingAddressId, shippingCode, additionalInstruction);
                if (cartViewModel.ShippingCost == 0 && cartViewModel.Shipping.ShippingDiscount != 0)
                {
                    cartViewModel.Shipping.ShippingDiscount = 0;
                }

                string shipcost = shippingCodeval[0].Replace("$", string.Empty).Trim();
                if (shippingCodeval.Length == 1)
                {
                    shipcost = "0";
                    cartViewModel.FreeShipping = true;
                }
                if (shipcost.Contains("Free"))
                {

                    shipcost = shipcost.Replace("(Free)", "");
                    cartViewModel.FreeShipping = true;
                }
                decimal shippingcost = 0;
                if (shipcost != "undefined")
                {
                    shippingcost = Convert.ToDecimal(shipcost);
                }

                if (cartViewModel.ShippingCost != shippingcost)
                {
                    if (cartViewModel.ErrorMessage == "Unable to calculate shipping rates at this time, please try again later.")
                    {
                        cartViewModel.ErrorMessage = "";
                    }

                    cartViewModel.Total = cartViewModel.Total - cartViewModel.ShippingCost;
                    cartViewModel.Total = cartViewModel.Total + shippingcost;
                    cartViewModel.ShippingCost = shippingcost;
                    ShoppingCartModel cartModel = GetFromSession<ShoppingCartModel>(WebStoreConstants.CartModelSessionKey) ?? GetCartFromCookie();
                    cartModel.ShippingCost = cartViewModel.ShippingCost;
                    cartModel.TaxCost = cartViewModel.TaxCost;
                    cartModel.Total = cartViewModel.Total;
                    SaveInSession(WebStoreConstants.CartModelSessionKey, cartModel);
                }

                return cartViewModel;
            }
        }

    }
}
