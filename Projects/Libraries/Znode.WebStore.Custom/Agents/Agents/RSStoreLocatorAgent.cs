﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;
using Znode.Engine.WebStore.Agents;
using Znode.WebStore.Custom.Agents.IAgents;

namespace Znode.WebStore.Custom.Agents.Agents
{
    public class RSStoreLocatorAgent : StoreLocatorAgent, IRSStoreLocatorAgent
    {
        #region Public Constructor
        public RSStoreLocatorAgent(IWebStoreLocatorClient webStoreLocatorClient) : base(webStoreLocatorClient)
        {

        }
        #endregion

        public void SaveInCookie(string storeId, string storeName, string storeAddress)
        {
            string defaultSelectedStoreDetails = storeId + "*" + storeName + "*" + storeAddress;
            SaveInCookie("DefaultSelectedStore", defaultSelectedStoreDetails);
        }

        public string GetCookie()
        {
            string _cookie = GetFromCookie("DefaultSelectedStore");
            return _cookie;
        }
    }
}
