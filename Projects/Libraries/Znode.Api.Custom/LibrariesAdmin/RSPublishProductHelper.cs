﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Libraries.Admin;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Api.Custom.LibrariesAdmin
{
    public class RSPublishProductHelper : PublishProductHelper
    {
        private readonly IMongoRepository<ProductEntity> _ProductMongoRepository;

        public RSPublishProductHelper():base()
        {
            _ProductMongoRepository = new MongoRepository<ProductEntity>();
           
        }
        public override List<List<AttributeEntity>> GetConfigurableAttributes(List<ProductEntity> productList, List<string> ConfigurableAttributeCodes = null)
        {        
                      
            if (productList?.Count > 0)
            {

                //Get configurable product Attribute ,//assigned display order of product to configurable attribute to display it on webstore depend on display order.
                // IEnumerable<AttributeEntity> Attributes = productList.SelectMany(x => x.Attributes?.Where(y => y.IsConfigurable && !string.IsNullOrEmpty(y.AttributeValues) && ConfigurableAttributeCodes.Contains(y.AttributeCode)).Select(y => { y.SelectValues.FirstOrDefault().DisplayOrder = x?.DisplayOrder; return y; }));
                //return  Attributes.GroupBy(u => u.AttributeCode).Select(grp => grp.ToList()).ToList();
                IEnumerable<AttributeEntity> Attributes = productList.SelectMany(x => x.Attributes?.Where(y => y.IsConfigurable && !string.IsNullOrEmpty(y.AttributeValues) && ConfigurableAttributeCodes.Contains(y.AttributeCode)));
                List<List<AttributeEntity>> lst = Attributes.GroupBy(u => u.AttributeCode).Select(grp => grp.ToList()).ToList();
                
                return lst;
            }
            return null;
        }
        public override List<ProductEntity> GetAssociatedProducts(int productId, int localeId, int? catalogVersionId, List<ConfigurableProductEntity> configEntity)
        {
            //Check if entity is not null.
            if (HelperUtility.IsNotNull(configEntity))
            {
                FilterCollection filters = GetConfigurableProductFilter(localeId, configEntity, catalogVersionId);
                //Get associated product list.
                List<ProductEntity> associatedProducts = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection())).GroupBy(g => g.SKU).Select(s => s.FirstOrDefault()).ToList();

                List<ProductEntity> newassociatedProducts = new List<ProductEntity>();

                //Assign Display order to associated product list.
                associatedProducts.ForEach(d =>
                {
                    ConfigurableProductEntity configurableProductEntity = configEntity
                                .FirstOrDefault(s => s.AssociatedZnodeProductId == d.ZnodeProductId);
                    d.DisplayOrder = HelperUtility.IsNotNull(configurableProductEntity) ? configurableProductEntity.AssociatedProductDisplayOrder : 999;
                    foreach (AttributeEntity attribute in d.Attributes?.Where(x => x.IsConfigurable == true))
                    {
                        attribute.AttributeValues = attribute.SelectValues.FirstOrDefault()?.Value;
                        /*NIVI CODE*/
                        attribute.DisplayOrder = attribute.AttributeCode=="Color"? attribute.DisplayOrder: Convert.ToInt32(attribute.SelectValues.FirstOrDefault()?.DisplayOrder);
                        /*NIVI CODE*/
                    }
                    newassociatedProducts.Add(d);
                });

                //Sort list according to display order.
                newassociatedProducts = newassociatedProducts.OrderBy(x => x.DisplayOrder)?.ToList();

                return newassociatedProducts;
            }
            return null;
        }

    }
}
