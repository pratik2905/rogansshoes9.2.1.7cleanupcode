﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Libraries.Admin;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
using static Znode.Libraries.ECommerce.Utilities.ZnodeDependencyResolver;
namespace Znode.Api.Custom.ShoppingCart
{
    public class RSZnodeShoppingCart : ZnodeShoppingCart
    {
        private readonly IPublishProductHelper publishProductHelper;
        public RSZnodeShoppingCart()
        {
            publishProductHelper = GetService<IPublishProductHelper>();

        }
        protected override void BindProductDetails(ZnodeShoppingCartItem znodeCartItem, PublishProductModel productModel, string parentSKu = null, int userId = 0, int omsOrderId = 0, decimal? unitPrice = null, string parentSKUProductName = null, int profileId = 0)
        {
            int catalogVersionId = GetCatalogVersionId(productModel.PublishedCatalogId, productModel.LocaleId);
            ProductEntity product = publishProductHelper.GetPublishProductBySKU(productModel.SKU, productModel.PublishedCatalogId, productModel.LocaleId, catalogVersionId, omsOrderId);
            if (IsNotNull(product) && IsNotNull(znodeCartItem))
            {
                bool isGroupProduct = productModel.GroupProductSKUs.Count > 0;
                string countryCode = znodeCartItem.ShippingAddress?.CountryName;
                PublishProductModel publishProduct = product.ToModel<PublishProductModel>();
                publishProduct.GroupProductSKUs = productModel.GroupProductSKUs;
                publishProduct.ConfigurableProductId = productModel.ParentPublishProductId;
                ZnodeProduct baseProduct = GetProductDetails(publishProduct, this.PortalId.GetValueOrDefault(), productModel.LocaleId, catalogVersionId, znodeCartItem.ShippingAddress?.CountryName, isGroupProduct, parentSKu, userId, omsOrderId, parentSKUProductName, profileId);
                znodeCartItem.ProductCode = product.Attributes.Where(x => x.AttributeCode == ZnodeConstant.ProductCode)?.FirstOrDefault()?.AttributeValues;
                znodeCartItem.ProductType = product.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductType)?.SelectValues?.FirstOrDefault()?.Code;
                znodeCartItem.Product = new ZnodeProductBase(baseProduct, znodeCartItem.ShippingAddress, unitPrice);
                znodeCartItem.Product.ZNodeAddonsProductCollection = GetZnodeProductAddons(productModel, productModel.PublishedCatalogId, productModel.LocaleId, baseProduct.AddOns, countryCode, userId, omsOrderId, profileId, catalogVersionId);
                znodeCartItem.Product.ZNodeBundleProductCollection = GetZnodeProductBundles(productModel.BundleProductSKUs, productModel.PublishedCatalogId, productModel.LocaleId, countryCode, omsOrderId, profileId, catalogVersionId);
                znodeCartItem.Product.ZNodeConfigurableProductCollection = GetZnodeProductConfigurables(productModel.ConfigurableProductSKUs, productModel.PublishedCatalogId, productModel.LocaleId, countryCode, productModel.ParentPublishProductId, userId, omsOrderId, profileId, productModel.Quantity.GetValueOrDefault(), catalogVersionId);
                znodeCartItem.Product.ZNodeGroupProductCollection = GetZnodeProductGroup(productModel.GroupProductSKUs, productModel.PublishedCatalogId, productModel.LocaleId, countryCode, userId, omsOrderId, profileId, catalogVersionId);
                znodeCartItem.Quantity = GetProductQuantity(znodeCartItem, productModel.Quantity.GetValueOrDefault());
                znodeCartItem.ParentProductId = productModel.ParentPublishProductId;
                znodeCartItem.UOM = baseProduct.UOM;
                znodeCartItem.ParentProductSKU = znodeCartItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group)
                                       ? znodeCartItem.ParentProductSKU : product.SKU;
                znodeCartItem.Product.SKU = !string.IsNullOrEmpty(parentSKu) && (!string.IsNullOrEmpty(productModel.ConfigurableProductSKUs) || isGroupProduct) ? parentSKu : product.SKU;
                //Nivi Image Changes start
                znodeCartItem.Image = znodeCartItem.Product.ZNodeGroupProductCollection?.Count > 0 ? znodeCartItem.Product.ZNodeGroupProductCollection[0].Attributes?.Where(x => x.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValue : product.Attributes?.Where(x => x.AttributeCode == "BaseImage")?.FirstOrDefault()?.AttributeValues;
                ZnodeLogging.LogMessage("RSZnodeShoppingCart - shopping cart" + znodeCartItem.Image, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                //Nivi Image Changes End
                znodeCartItem.Product.Container = GetAttributeValueByCode(znodeCartItem, product, ZnodeConstant.ShippingContainer);
                znodeCartItem.Product.Size = GetAttributeValueByCode(znodeCartItem, product, ZnodeConstant.ShippingSize);
                znodeCartItem.Product.PackagingType = product.Attributes.Where(x => x.AttributeCode == ZnodeConstant.PackagingType)?.FirstOrDefault()?.SelectValues[0]?.Value;
                znodeCartItem.Product.DownloadableProductKey = GetProductKey(znodeCartItem.Product.SKU, znodeCartItem.Quantity, znodeCartItem.OmsOrderLineItemId);
                znodeCartItem.AssociatedAddOnProducts = productModel.AssociatedAddOnProducts;
                SetInventoryData(znodeCartItem.Product);
            }
        }

        protected override void BindProductDetailsV2(ZnodeShoppingCartItem znodeCartItem, PublishProductModel productModel, PublishProductModel publishProduct, 
            string parentSKu = null, int userId = 0, int omsOrderId = 0, decimal? unitPrice = null, string parentSKUProductName = null, 
            int profileId = 0, int catalogVersionId = 0, List<PublishProductModel> publishProducts = null, List<TaxClassRuleModel> lstTaxClassSKUs = null,
            List<ZnodePimDownloadableProduct> lstDownloadableProducts = null, List<ConfigurableProductEntity> configEntities = null)
        {
            if (IsNotNull(publishProduct) && IsNotNull(znodeCartItem))
            {
                bool isGroupProduct = productModel.GroupProductSKUs.Count > 0;
                string countryCode = znodeCartItem.ShippingAddress?.CountryName;
                publishProduct.GroupProductSKUs = productModel.GroupProductSKUs;
                publishProduct.ConfigurableProductId = productModel.ParentPublishProductId;
                publishProduct.ParentPublishProductId = productModel.ParentPublishProductId;
                productModel.PublishProductId = publishProduct.PublishProductId;
                publishProduct.SEOUrl = productModel.ParentPublishProductId > 0 ? publishProducts.FirstOrDefault(x => x.PublishProductId == productModel.ParentPublishProductId)?.SEOUrl : publishProduct.SEOUrl;
                ZnodeProduct baseProduct = GetProductDetailsV2(publishProduct, this.PortalId.GetValueOrDefault(), productModel.LocaleId, znodeCartItem.ShippingAddress?.CountryName, isGroupProduct, parentSKu, userId, omsOrderId, parentSKUProductName, profileId, lstTaxClassSKUs, configEntities);
                znodeCartItem.ProductCode = publishProduct.Attributes.Where(x => x.AttributeCode == ZnodeConstant.ProductCode)?.FirstOrDefault()?.AttributeValues;
                znodeCartItem.ProductType = publishProduct.Attributes.FirstOrDefault(x => x.AttributeCode == ZnodeConstant.ProductType)?.SelectValues?.FirstOrDefault()?.Code;
                znodeCartItem.Product = new ZnodeProductBase(baseProduct, znodeCartItem.ShippingAddress, unitPrice);
                znodeCartItem.Product.ZNodeAddonsProductCollection = GetZnodeProductAddons(productModel, productModel.PublishedCatalogId, productModel.LocaleId, baseProduct.AddOns, countryCode, userId, omsOrderId, profileId, catalogVersionId);
                znodeCartItem.Product.ZNodeBundleProductCollection = GetZnodeProductBundles(productModel.BundleProductSKUs, productModel.PublishedCatalogId, productModel.LocaleId, countryCode, omsOrderId, profileId, catalogVersionId);
                znodeCartItem.Product.ZNodeConfigurableProductCollection = GetZnodeProductConfigurables(productModel.ConfigurableProductSKUs, productModel.PublishedCatalogId, productModel.LocaleId, countryCode, productModel.ParentPublishProductId, userId, omsOrderId, profileId, productModel.Quantity.GetValueOrDefault(), catalogVersionId, publishProducts, lstTaxClassSKUs, configEntities);
                znodeCartItem.Product.ZNodeGroupProductCollection = GetZnodeProductGroup(productModel.GroupProductSKUs, productModel.PublishedCatalogId, productModel.LocaleId, countryCode, userId, omsOrderId, profileId, catalogVersionId);
                znodeCartItem.Quantity = GetProductQuantity(znodeCartItem, productModel.Quantity.GetValueOrDefault());
                znodeCartItem.ParentProductId = productModel.ParentPublishProductId;
                znodeCartItem.UOM = baseProduct.UOM;
                znodeCartItem.ParentProductSKU = znodeCartItem.OrderLineItemRelationshipTypeId == Convert.ToInt32(ZnodeCartItemRelationshipTypeEnum.Group)
                                       ? znodeCartItem.ParentProductSKU : publishProduct.SKU;
                znodeCartItem.Product.SKU = !string.IsNullOrEmpty(parentSKu) && (!string.IsNullOrEmpty(productModel.ConfigurableProductSKUs) || isGroupProduct) ? parentSKu : publishProduct.SKU;
                //Nivi Image Changes start               
                znodeCartItem.Image = znodeCartItem.Product.ZNodeGroupProductCollection?.Count > 0 ? znodeCartItem.Product.ZNodeGroupProductCollection[0].Attributes?.Where(x => x.AttributeCode == ZnodeConstant.ProductImage)?.FirstOrDefault()?.AttributeValue : publishProduct.Attributes?.Where(x => x.AttributeCode == "BaseImage")?.FirstOrDefault()?.AttributeValues;                
                //Nivi Image Changes End
               
                znodeCartItem.Product.Container = GetAttributeValueByCode(znodeCartItem, publishProduct, ZnodeConstant.ShippingContainer);
                znodeCartItem.Product.Size = GetAttributeValueByCode(znodeCartItem, publishProduct, ZnodeConstant.ShippingSize);
                znodeCartItem.Product.PackagingType = publishProduct.Attributes.Where(x => x.AttributeCode == ZnodeConstant.PackagingType)?.FirstOrDefault()?.SelectValues[0]?.Value;
                znodeCartItem.Product.DownloadableProductKey = GetProductKey(znodeCartItem.Product.SKU, znodeCartItem.Quantity, znodeCartItem.OmsOrderLineItemId, lstDownloadableProducts);
                znodeCartItem.AssociatedAddOnProducts = productModel.AssociatedAddOnProducts;
                SetInventoryData(znodeCartItem.Product);
            }
        }

    }
}