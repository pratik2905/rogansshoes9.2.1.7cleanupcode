﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Api.Custom.Service.IService
{
    public interface IRSOrderAPIService
    {
        string ExecutePackage();
        bool UpdateOrderStatusMany(OrderLineItemDataListModel orderDetailListModel);       
    }
}
