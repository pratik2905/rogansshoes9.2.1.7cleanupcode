﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.Service.IService;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Libraries.Admin;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.MongoDB.Data;

namespace Znode.Api.Custom.Service.Service
{
    public class RSPublishProductService : PublishProductService, IRSPublishProductService
    {
        private readonly IMongoRepository<ConfigurableProductEntity> _configurableproductRepository;
        private readonly IMongoRepository<ProductEntity> _ProductMongoRepository;
        private readonly IZnodeRepository<ZnodePublishProduct> _publishProductRepository;
        private readonly IPublishProductHelper publishProductHelper;
        private readonly IMongoRepository<CategoryEntity> _categoryMongoRepository;
        private readonly ISEOService _seoService;


        #region Constructor
        public RSPublishProductService() : base()
        {
            _ProductMongoRepository = new MongoRepository<ProductEntity>();
            _configurableproductRepository = new MongoRepository<ConfigurableProductEntity>();
            _publishProductRepository = new ZnodeRepository<ZnodePublishProduct>();
            publishProductHelper = ZnodeDependencyResolver.GetService<IPublishProductHelper>();
            _categoryMongoRepository = new MongoRepository<CategoryEntity>(GetCatalogVersionId());
            _seoService = new SEOService();

        }
        #endregion
        public override PublishProductModel GetPublishProduct(int publishProductId, FilterCollection filters, NameValueCollection expands)
        {

            GetParametersValueForFilters(filters, out int catalogId, out int portalId, out int localeId);
            int versionId = GetCatalogVersionId(catalogId, localeId).GetValueOrDefault();

            PublishProductModel publishProduct = base.GetPublishProduct(publishProductId, filters, expands);
            return publishProduct;
        }

        public override PublishProductModel GetConfigurableProduct(ParameterProductModel productAttributes, NameValueCollection expands)
        {
            try
            {

                PublishProductModel product = null;

                IMongoQuery query = Query<ConfigurableProductEntity>.EQ(pr => pr.ZnodeProductId, productAttributes.ParentProductId);
                int? versionId = GetCatalogVersionId(productAttributes.PublishCatalogId);

                query = Query.And(query, Query<ConfigurableProductEntity>.EQ(x => x.VersionId, versionId));

                List<ConfigurableProductEntity> configEntity = _configurableproductRepository.GetEntityList(query, true);

                FilterCollection filters = GetConfigurableProductFilter(productAttributes.LocaleId, configEntity);
                filters.Add(WebStoreEnum.VersionId.ToString(), FilterOperators.Equals, versionId.ToString());

                List<IMongoQuery> mongoQuery;


                List<ProductEntity> productList = _ProductMongoRepository.GetEntityList(MongoQueryHelper.GenerateDynamicWhereClause(filters.ToFilterMongoCollection()), true);
                List<ProductEntity> associatedProducts = productList;



                foreach (KeyValuePair<string, string> item in productAttributes.SelectedAttributes)
                {
                    List<ProductEntity> _newProductList = new List<ProductEntity>();
                    productList.ForEach(x =>
                    {
                        AttributeEntity _Attributes = x.Attributes.FirstOrDefault(y => y.AttributeCode == item.Key && y.SelectValues.FirstOrDefault()?.Value == item.Value);

                        if (HelperUtility.IsNotNull(_Attributes))
                        {
                            _newProductList.Add(x);
                        }
                    });
                    productList = _newProductList;
                }

                ProductEntity productEntity = productList.FirstOrDefault();

                //If Combination does not exist.
                if (HelperUtility.IsNull(productEntity))
                {
                    //Get filter

                    filters = GetConfigurableProductFilter(productAttributes.LocaleId, configEntity);
                    productAttributes.SelectedAttributes = new Dictionary<string, string>();
                    mongoQuery = GetMongoQueryForConfigurableProduct(productAttributes, filters);

                    //Get Default product.
                    productEntity = _ProductMongoRepository.GetEntity(Query.And(mongoQuery));
                    product = productEntity?.ToModel<PublishProductModel>();
                    //Set IsDefaultConfigurableProduct flag.
                    if (HelperUtility.IsNotNull(product))
                    {
                        product.IsDefaultConfigurableProduct = true;
                    }

                }
                else
                {
                    product = productEntity?.ToModel<PublishProductModel>();
                }

                if (HelperUtility.IsNotNull(product))
                {

                    product.AssociatedGroupProducts = productList.ToModel<WebStoreGroupProductModel>().ToList();

                    product.ConfigurableProductId = productAttributes.ParentProductId;
                    product.IsConfigurableProduct = true;

                    product.ProductType = ZnodeConstant.ConfigurableProduct;
                    product.ConfigurableProductSKU = product.SKU;
                    product.SKU = productAttributes.ParentProductSKU;

                    publishProductHelper.GetDataFromExpands(productAttributes.PortalId, GetExpands(expands), product, productAttributes.LocaleId, string.Empty, GetLoginUserId(), null, null, GetProfileId());

                    GetProductImagePath(productAttributes.PortalId, product);
                    //set stored based In Stock, Out Of Stock, Back Order Message.
                    SetPortalBasedDetails(productAttributes.PortalId, product);

                    /*Nivi Code*/
                    //Nivi code start for image changes
                    string _swatchImagePath = ConfigurationManager.AppSettings["BaseImagePath"].ToString() + "t_97/";
                    List<AssociatedProductsModel> _products = (from p in associatedProducts

                                                               select new AssociatedProductsModel
                                                               {
                                                                   PublishProductId = p.ZnodeProductId,

                                                                   SKU = p.SKU,
                                                                   OMSColorCode = p.Attributes.Where(x => x.AttributeCode == ZnodeConstant.GalleryImages)?.FirstOrDefault()?.AttributeValues,
                                                                   OMSColorValue = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Color")?.SelectValues[0].Code,
                                                                   OMSColorPath = _swatchImagePath + p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues,
                                                                   DisplayOrder = p.DisplayOrder,
                                                                   OMSColorSwatchText = p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues,
                                                                   Custom1 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Size")?.SelectValues[0].Code,
                                                                   Custom2 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "ShoeWidth")?.SelectValues[0].Code
                                                               }).ToList();
                    ZnodeLogging.LogMessage("RSPublishProductService-_swatch Image Path Product line 179" + _products, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
                    //Nivi code end for image changes
                    product.AssociatedProducts = new List<AssociatedProductsModel>();
                    product.AssociatedProducts.AddRange(_products);
                    string[] codes = productAttributes.Codes.Split(',');
                    int colorIndex = Array.IndexOf(codes, "Color");
                    string selectedcolor = productAttributes.Values.Split(',')?[colorIndex];
                    string prdImage = product.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == selectedcolor)?.OMSColorSwatchText;
                    ZnodeLogging.LogMessage("prdImage for selected color=" + prdImage, ZnodeLogging.Components.API.ToString(), TraceLevel.Error);
                    ZnodeLogging.LogMessage("prdImage for product from base=" + product.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues, ZnodeLogging.Components.API.ToString(), TraceLevel.Error);
                    if (prdImage != product.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues)
                    {
                        GetProductImagesforUnavailableProduct(product, productAttributes.PortalId, selectedcolor);
                    }

                    /*Nivi Code*/
                }
                return product;
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("RSPublishProductService-Error-" + ex.Message + "- Stack Trace - " + ex.StackTrace, ZnodeLogging.Components.API.ToString(), TraceLevel.Error);
                return new PublishProductModel();
            }
        }

        public void GetProductImagesforUnavailableProduct(PublishProductModel product, int portalId, string color)
        {

            string prdImage = product.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorSwatchText;
            ImageHelper imageHelper = new ImageHelper(portalId);
            product.ImageLargePath = imageHelper.GetImageHttpPathLarge(prdImage);
            product.ImageMediumPath = imageHelper.GetImageHttpPathMedium(prdImage);
            product.ImageThumbNailPath = imageHelper.GetImageHttpPathThumbnail(prdImage);
            product.ImageSmallPath = imageHelper.GetImageHttpPathSmall(prdImage);
            product.OriginalImagepath = imageHelper.GetOriginalImagepath(prdImage);
            string alternateImages = product.AssociatedProducts.FirstOrDefault(w => w.OMSColorValue == color)?.OMSColorCode;
            if (!string.IsNullOrEmpty(alternateImages))
            {
                List<ProductAlterNateImageModel> mediaAttributeList = GetGalleryImageMediaAttributes(alternateImages);
                product.AlternateImages = new List<ProductAlterNateImageModel>();
                string[] images = alternateImages.Split(',');
                foreach (string image1 in images)
                {
                    string imageAltText = mediaAttributeList?.FirstOrDefault(x => x.FileName == image1 && x.ImageSmallThumbNail == "Display Name")?.ImageSmallPath;
                    string displayOrder = mediaAttributeList?.FirstOrDefault(x => x.FileName == image1 && x.ImageSmallThumbNail == "ImageDisplayOrderNumber")?.ImageSmallPath;
                    product.AlternateImages.Add(new ProductAlterNateImageModel { FileName = image1, ImageSmallPath = imageAltText, ImageThumbNailPath = displayOrder, OriginalImagePath = imageHelper.GetOriginalImagepath(image1), ImageLargePath = imageHelper.GetImageHttpPathLarge(image1) });

                }
            }

        }

        private List<ProductAlterNateImageModel> GetGalleryImageMediaAttributes(string alternateImages)
        {
            IZnodeViewRepository<ProductAlterNateImageModel> objStoredProc = new ZnodeViewRepository<ProductAlterNateImageModel>();
            objStoredProc.SetParameter("@media", alternateImages, ParameterDirection.Input, DbType.String);

            //Data on the basis of product skus and product ids
            return objStoredProc.ExecuteStoredProcedureList("RS_GetGalleryImageMediaAttributes @media").ToList();
        }

        public DataTable GetStoreLocations(string state, string sku)
        {
            ExecuteSpHelper objStoredProc = new ExecuteSpHelper();
            objStoredProc.GetParameter("@State", state, ParameterDirection.Input, SqlDbType.VarChar);
            objStoredProc.GetParameter("@SKU", sku, ParameterDirection.Input, SqlDbType.VarChar);

            DataSet data = objStoredProc.GetSPResultInDataSet("RS_GetStoreAddressDetails");

            DataTable storeLocationTable = data.Tables[0];

            return storeLocationTable;
        }

        public DataTable GetStoreLocationDetails(string state, string sku)
        {
            DataTable storeData = this.GetStoreLocations(state, sku);
            return storeData;
        }

        private PublishProductModel AddPersonalizeAttributeInChildProduct(PublishProductModel publishProduct, List<PublishAttributeModel> parentPersonalizableAttributes, bool isChildPersonalizableAttribute)
        {
            if (!isChildPersonalizableAttribute)
            {
                if (parentPersonalizableAttributes?.Count > 0)
                {
                    foreach (PublishAttributeModel parentPersonalizableAttribute in parentPersonalizableAttributes)
                    {
                        publishProduct.Attributes.Add(parentPersonalizableAttribute);
                    }
                }
            }

            return publishProduct;
        }


        protected override PublishProductModel GetDefaultConfiurableProduct(NameValueCollection expands, int portalId, int localeId, PublishProductModel publishProduct, List<ProductEntity> associatedProducts, List<string> ConfigurableAttributeCodes = null, int? catalogVersionId = 0)
        {

            //ZnodeLogging.LogMessage("RSPUBLISHPRODUCTSERVICE-GETDEFAULTCONFIURABLEPRODUCT STARTED." + Convert.ToString(DateTime.Now), ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);

            string sku = publishProduct.SKU;
            string parentSEOCode = publishProduct.Attributes?.FirstOrDefault(x => x.AttributeCode == "SKU")?.AttributeValues;
            int categoryIds = publishProduct.ZnodeCategoryIds;
            List<PublishCategoryModel> categoryHierarchyIds = publishProduct.CategoryHierarchy;

            List<PublishAttributeModel> parentPersonalizableAttributes = publishProduct.Attributes?.Where(x => x.IsPersonalizable).ToList();
            string parentConfigurableProductName = publishProduct.Name;
            int configurableProductId = publishProduct.PublishProductId;

            List<ProductEntity> newassociatedProducts = new List<ProductEntity>();
            foreach (ProductEntity product in associatedProducts)
            {
                foreach (AttributeEntity attributeEntity in product.Attributes)
                {
                    if (attributeEntity.IsConfigurable)
                    {
                        attributeEntity.AttributeValues = attributeEntity.SelectValues.FirstOrDefault()?.Value;
                        /*NIVI CODE*/
                        if (attributeEntity.AttributeCode == "Color")
                        {
                            //assigned display order of product to configurable attribute to display it on webstore depend on display order.
                            if (attributeEntity?.SelectValues?.Count > 0)
                                attributeEntity.SelectValues.FirstOrDefault().DisplayOrder = product?.DisplayOrder;

                            attributeEntity.DisplayOrder = product.DisplayOrder;
                        }
                        else
                        {

                            if (attributeEntity?.SelectValues?.Count > 0)
                            {
                                //attributeEntity.SelectValues.FirstOrDefault().DisplayOrder = product?.DisplayOrder;
                                attributeEntity.DisplayOrder = Convert.ToInt32(attributeEntity.SelectValues.FirstOrDefault().DisplayOrder);
                            }
                        }
                        /*NIVI CODE*/
                    }
                }
                newassociatedProducts.Add(product);
            }
            //foreach (ProductEntity product in associatedProducts)
            //{
            //    foreach (AttributeEntity attributeEntity in product.Attributes)
            //    {
            //        if (attributeEntity.IsConfigurable)
            //        {
            //            attributeEntity.AttributeValues = attributeEntity.SelectValues.FirstOrDefault()?.Value;
            //            //assigned display order of product to configurable attribute to display it on webstore depend on display order.
            //            if (attributeEntity?.SelectValues?.Count > 0)
            //            {
            //                attributeEntity.SelectValues.FirstOrDefault().DisplayOrder = product?.DisplayOrder;
            //            }

            //            attributeEntity.DisplayOrder = product.DisplayOrder;
            //        }
            //    }
            //    newassociatedProducts.Add(product);
            //}
            //Get first product from list of associated products 
            publishProduct = newassociatedProducts.FirstOrDefault().ToModel<PublishProductModel>();

            publishProduct.ConfigurableProductId = configurableProductId;
            publishProduct.ParentSEOCode = parentSEOCode;
            publishProduct.ConfigurableProductSKU = publishProduct.SKU;
            publishProduct.CategoryHierarchy = categoryHierarchyIds;

            publishProduct.SKU = sku;
            publishProduct.ZnodeCategoryIds = categoryIds;
            publishProduct.IsConfigurableProduct = true;
            publishProduct.ParentConfiguarableProductName = parentConfigurableProductName;

            catalogVersionId = Convert.ToInt32(catalogVersionId) > 0 ? catalogVersionId : GetCatalogVersionId();

            publishProductHelper.GetDataFromExpands(portalId, GetExpands(expands), publishProduct, localeId, WhereClauseForPortalId(portalId), GetLoginUserId(), catalogVersionId, WebstoreVersionId, GetProfileId());

            PublishAttributeModel defaultAttribute = publishProduct.Attributes?.FirstOrDefault(x => x.IsConfigurable);
            bool isChildPersonalizableAttribute = Convert.ToBoolean(publishProduct.Attributes?.Contains(publishProduct.Attributes?.FirstOrDefault(x => x.IsPersonalizable)));

            List<PublishAttributeModel> variants = publishProduct?.Attributes?.Where(x => x.IsConfigurable).ToList();
            Dictionary<string, string> selectedAttribute = GetSelectedAttributes(variants);

            List<PublishAttributeModel> attributeList = MapWebStoreConfigurableAttributeData(publishProductHelper.GetConfigurableAttributes(newassociatedProducts, ConfigurableAttributeCodes), defaultAttribute?.AttributeCode, defaultAttribute?.AttributeValues, selectedAttribute, newassociatedProducts, ConfigurableAttributeCodes, portalId);

            foreach (PublishAttributeModel item in attributeList)
            {
                publishProduct.Attributes.RemoveAll(x => x.AttributeCode == item.AttributeCode);
            }

            publishProduct.Attributes.AddRange(attributeList);
            /*NIVI CODE*/
            string _swatchImagePath = ConfigurationManager.AppSettings["BaseImagePath"].ToString() + "t_97/";
            ZnodeLogging.LogMessage("RSPublishProductService -_swatch Image Path line 379" + _swatchImagePath, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            //int index = 0;
            //if (publishProduct.ImageThumbNailPath == null)
            //{
            //    ImageHelper image = new ImageHelper(portalId);
            //    string productImage = image.GetImageHttpPathThumbnail("image"); ;
            //    index = productImage.LastIndexOf('/');
            //    if (index != -1)
            //    {
            //        _swatchImagePath = productImage.Substring(0, index) + "/";
            //    }
            //}
            //else
            //{
            //    index = publishProduct.ImageThumbNailPath.LastIndexOf('/');
            //    if (index != -1)
            //    {
            //        _swatchImagePath = publishProduct.ImageThumbNailPath.Substring(0, index) + "/";
            //    }
            //}
            List<AssociatedProductsModel> _products = (from p in associatedProducts

                                                       select new AssociatedProductsModel
                                                       {
                                                           PublishProductId = p.ZnodeProductId,
                                                           //PimProductId = pimProducts.FirstOrDefault(w => w.PublishProductId == p.ZnodeProductId)?.PimProductId,
                                                           SKU = p.SKU,
                                                           OMSColorCode = p.Attributes.Where(x => x.AttributeCode == ZnodeConstant.GalleryImages)?.FirstOrDefault()?.AttributeValues,
                                                           OMSColorValue = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Color")?.SelectValues[0].Code,
                                                           //OMSColorPath = _swatchImagePath + p.Attributes.FirstOrDefault(y => y.AttributeCode == ZnodeConstant.ProductImage)?.AttributeValues,
                                                           OMSColorPath = _swatchImagePath + p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues,
                                                           DisplayOrder = p.DisplayOrder,
                                                           OMSColorSwatchText = p.Attributes.FirstOrDefault(y => y.AttributeCode == "BaseImage")?.AttributeValues,
                                                           Custom1 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "Size")?.SelectValues[0].Code,
                                                           Custom2 = p.Attributes.FirstOrDefault(y => y.AttributeCode == "ShoeWidth")?.SelectValues[0].Code
                                                       }).ToList();
            ZnodeLogging.LogMessage("RSPublishProductService -_swatch Image Path line 415" + _products, ZnodeLogging.Components.PIM.ToString(), TraceLevel.Error);
            publishProduct.AssociatedProducts = new List<AssociatedProductsModel>();
            publishProduct.AssociatedProducts.AddRange(_products);
            /*NIVI CODE*/
            return AddPersonalizeAttributeInChildProduct(publishProduct, parentPersonalizableAttributes, isChildPersonalizableAttribute);

        }

    }
}
