﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Znode.Api.Custom.ShoppingCart;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Libraries.Data;
using Znode.Libraries.Data.DataModel;
using Znode.Libraries.Data.Helpers;
using Znode.Libraries.ECommerce.Fulfillment;
using Znode.Libraries.ECommerce.ShoppingCart;
using Znode.Libraries.ECommerce.Utilities;
using Znode.Libraries.Framework.Business;
using Znode.Libraries.Observer;
using Znode.Libraries.Resources;
using static Znode.Libraries.ECommerce.Utilities.HelperUtility;
namespace Znode.Api.Custom.Service.Service
{
    public class RSOrderService : OrderService
    {
        private decimal thresholdShippingValue = Convert.ToDecimal(ConfigurationManager.AppSettings["ThresholdShippingValue"]);
        private readonly IZnodeRepository<ZnodeOmsOrderLineItem> _orderLineItemRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderShipment> _orderOrderShipmentRepository;
        private readonly IZnodeRepository<ZnodeOmsOrderDetail> _orderOrderDetailRepository;
        #region Constructor

        public RSOrderService() : base()
        {
            _orderLineItemRepository = new ZnodeRepository<ZnodeOmsOrderLineItem>();
            _orderOrderShipmentRepository = new ZnodeRepository<ZnodeOmsOrderShipment>();
            _orderOrderDetailRepository = new ZnodeRepository<ZnodeOmsOrderDetail>();
        }

        #endregion Constructor
        public override IZnodeCheckout SetShoppingCartDataToCheckout(IZnodeCheckout checkout, ShoppingCartModel model)
        {
            IZnodeCheckout zCheckout = base.SetShoppingCartDataToCheckout(checkout, model);
            zCheckout.ShoppingCart.CustomShippingCost = model.ShippingCost;
            return zCheckout;
        }

        //Update order line items.


        #region Order Receipt Changes
        public override string GetOrderReceipt(ZnodeOrderFulfillment order, IZnodeCheckout checkout, string feedbackUrl, int localeId, bool isUpdate, out bool isEnableBcc)
        {
            foreach (OrderLineItemModel item in order.OrderLineItems)
            {
                if (item.PersonaliseValueList != null)
                {
                    item.PersonaliseValueList.Remove("AllocatedLineItems");
                }

                if (item.PersonaliseValuesDetail != null)
                {
                    item.PersonaliseValuesDetail.RemoveAll(pv => pv.PersonalizeCode == "AllocatedLineItems");
                }
            }

            RSZnodeOrderReceipt receipt = new RSZnodeOrderReceipt(order, checkout.ShoppingCart);

            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("OrderReceipt", (order.PortalId > 0) ? order.PortalId : PortalId, localeId);
            if (HelperUtility.IsNotNull(emailTemplateMapperModel))
            {
                string receiptContent = ShowOrderAdditionalDetails(emailTemplateMapperModel.Descriptions, order.Custom1);
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderReceiptHtml(receiptContent));

            }
            isEnableBcc = false;
            return string.Empty;
        }

        public override bool SendOrderReceipt(int portalId, string userEmailId, string subject, string senderEmail, string bccEmailId, string receiptHtml, bool isEnableBcc = false)
        {
            bool isSuccess = false;
            if (subject.Contains(Admin_Resources.TitleOrderReceipt))
                subject = Admin_Resources.SubjectOrderReceipt;
            ZnodeEmail.SendEmail(portalId, userEmailId, senderEmail, ZnodeEmail.GetBccEmail(isEnableBcc, portalId, bccEmailId), subject, receiptHtml, true);
            isSuccess = true;
            return isSuccess;
        }

        private string ShowOrderAdditionalDetails(string receiptContent, string customData)
        {
            if (!string.IsNullOrEmpty(customData))
            {
                return receiptContent.Replace("#FeedBack#", GenerateOrderAdditionalInfoTemplate(customData));
            }
            else
            {
                return receiptContent;
            }
        }
        //to generate order additional information template
        private string GenerateOrderAdditionalInfoTemplate(string customData)
        {
            string template = string.Empty;
            try
            {
                Dictionary<string, string> CustomDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(customData);
                if (HelperUtility.IsNotNull(CustomDict))
                {
                    template = ("<b>Additional Information</b>");

                    if (CustomDict.ContainsKey("ProductName"))
                    {
                        template += $" <br />Product will be used by  { CustomDict["ProductName"]}";
                    }

                    if (CustomDict.ContainsKey("RecipientName"))
                    {
                        template += $" <br />Recipient of the product {CustomDict["RecipientName"]}";
                    }

                    if (CustomDict.ContainsKey("ApproverManager"))
                    {
                        template += $" <br />Approving Manager {CustomDict["ApproverManager"]}";
                    }

                    if (CustomDict.ContainsKey("ProjectName"))
                    {
                        template += $" <br />Project Name {CustomDict["ProjectName"]}";
                    }

                    if (CustomDict.ContainsKey("EventDate"))
                    {
                        template += $" <br />Event Date {CustomDict["EventDate"]}";
                    }

                    if (CustomDict.ContainsKey("InHandsDate"))
                    {
                        template += $" <br />In Hands Date {CustomDict["InHandsDate"]}";
                    }
                }
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
            }
            return template;
        }

        #endregion

        #region Order EMail
        /// <summary>
        /// Override to remove extra space
        /// </summary>
        /// <param name="orderShipment"></param>
        /// <returns></returns>     
        public override string GetOrderShipmentAddress(OrderShipmentModel orderShipment)
        {
            if (IsNotNull(orderShipment))
            {
                string ShippingcompanyName = _addressRepository.Table.Where(x => x.AddressId == orderShipment.AddressId)?.FirstOrDefault()?.CompanyName;

                string street1 = string.IsNullOrEmpty(orderShipment.ShipToStreet2) ? string.Empty : "<br />" + orderShipment.ShipToStreet2;
                orderShipment.ShipToCompanyName = IsNotNull(orderShipment?.ShipToCompanyName) ? $"{orderShipment?.ShipToCompanyName}" : ShippingcompanyName;
                //return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{"<br />"}{ orderShipment.ShipToCompanyName}{"<br />"}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{"<br />"}{orderShipment.ShipToStateCode}{"<br />"}{orderShipment.ShipToPostalCode}{"<br />"}{orderShipment.ShipToCountry}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipment.ShipToPhoneNumber}{"<br />"}{WebStore_Resources.TitleEmail}{" : "}{orderShipment.ShipToEmailId}";
                if (orderShipment.ShipToCompanyName != null)
                {
                    //orderShipment.ShipToCompanyName =   orderShipment.ShipToCompanyName;
                    orderShipment.ShipToCompanyName = orderShipment?.ShipToCompanyName;
                }
                return $"{orderShipment?.ShipToFirstName}{" "}{ orderShipment?.ShipToLastName}{ orderShipment.ShipToCompanyName}{"<br />"}{orderShipment.ShipToStreet1}{street1}{"<br />"}{ orderShipment.ShipToCity}{"<br />"}{orderShipment.ShipToStateCode}{"<br />"}{orderShipment.ShipToPostalCode}{"<br />"}{orderShipment.ShipToCountry}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderShipment.ShipToPhoneNumber}{"<br />"}{WebStore_Resources.TitleEmail}{" : "}{orderShipment.ShipToEmailId}";
            }
            return string.Empty;
        }
        /// <summary>
        /// Override to remove extra space-REmove company name as it not get in list
        /// </summary>
        /// <param name="orderBilling"></param>
        /// <returns></returns>
        public override string GetOrderBillingAddress(OrderModel orderBilling)
        {
            if (IsNotNull(orderBilling))
            {
                string street1 = string.IsNullOrEmpty(orderBilling.BillingAddress.Address2) ? string.Empty : "<br />" + orderBilling.BillingAddress.Address2;
                string CompanyName = "";
                if (orderBilling?.BillingAddress.CompanyName != null)
                {
                    CompanyName = string.IsNullOrEmpty(orderBilling.BillingAddress.CompanyName) ? string.Empty : "<br />" + orderBilling.BillingAddress.CompanyName;
                }
                //return $"{orderBilling?.BillingAddress.FirstName}{" "}{orderBilling?.BillingAddress.LastName}{"<br />"}{orderBilling?.BillingAddress.CompanyName}{"<br />"}{orderBilling.BillingAddress.Address1}{street1}{"<br />"}{ orderBilling.BillingAddress.CityName}{"<br />"}{(string.IsNullOrEmpty(orderBilling.BillingAddress.StateCode) ? orderBilling.BillingAddress.StateName : orderBilling.BillingAddress.StateCode)}{"<br />"}{orderBilling.BillingAddress.PostalCode}{"<br />"}{orderBilling.BillingAddress.CountryName}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderBilling.BillingAddress.PhoneNumber}";
                return $"{orderBilling?.BillingAddress.FirstName}{" "}{orderBilling?.BillingAddress.LastName}{orderBilling.BillingAddress.CompanyName}{CompanyName}{"<br />"}{orderBilling.BillingAddress.Address1}{street1}{"<br />"}{ orderBilling.BillingAddress.CityName}{"<br />"}{(string.IsNullOrEmpty(orderBilling.BillingAddress.StateCode) ? orderBilling.BillingAddress.StateName : orderBilling.BillingAddress.StateCode)}{"<br />"}{orderBilling.BillingAddress.PostalCode}{"<br />"}{orderBilling.BillingAddress.CountryName}{"<br />"}{Admin_Resources.LabelPhoneNumber}{" : "}{orderBilling.BillingAddress.PhoneNumber}";
            }
            return string.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderModel"></param>
        public override void SendOrderStatusEmail(OrderModel orderModel)
        {
            ImageHelper imageHelper = new ImageHelper(orderModel.PortalId);
            foreach (OrderLineItemModel item in orderModel.OrderLineItems)
            {
                item.OrderLineItemCollection.AddRange(orderModel.OrderLineItems.Where(x => x.ParentOmsOrderLineItemsId == item.OmsOrderLineItemsId && x.OrderLineItemRelationshipTypeId != (int)ZnodeCartItemRelationshipTypeEnum.AddOns)?.ToList());
                item.ProductImagePath = "";
                if (item.DownloadLink !="")
                {
                    item.ProductImagePath = item.DownloadLink;
                    string images = imageHelper.GetImageHttpPathSmallThumbnail(Convert.ToString(item.ProductImagePath));
                    item.ProductImagePath = "<img src=\"" + images + "\" data-src=\"" + images + "\"/>";
                }
                
            }
            orderModel.OrderLineItems.RemoveAll(x => x.ParentOmsOrderLineItemsId == null);

            string subject = string.Empty;
            bool isEnableBcc = false;
            if (orderModel.OrderState == ZnodeOrderStatusEnum.CANCELLED.ToString())
            {
                //subject = $"{Admin_Resources.CancelledOrderStatusSubject} - {orderModel.OrderNumber}";
                subject = Admin_Resources.SubjectCancelReceipt;
                orderModel.ReceiptHtml = GetCancelledOrderReceiptForEmail(orderModel, out isEnableBcc);
            }
            else if (orderModel.OrderState == ZnodeOrderStatusEnum.SUBMITTED.ToString())
            {
                //subject = $"{Admin_Resources.OrderStatusUpdated} - {orderModel.OrderNumber}";
                subject = Admin_Resources.SubjectSubmittedReceipt;
                orderModel.ReceiptHtml = GetSubmittedReceiptForEmail(orderModel, out isEnableBcc);
            }
            else if (orderModel.OrderState == ZnodeOrderStatusEnum.SHIPPED.ToString())
            {
                // And finally attach the receipt HTML to the order and return
                // subject = $"{Admin_Resources.ShippedOrderStatusSubject} - {orderModel.OrderNumber}";
                subject = Admin_Resources.SubjectShippingReceipt;
                orderModel.ReceiptHtml = GetShippingReceiptForEmail(orderModel, out isEnableBcc);
            }

            SendOrderReceipt(orderModel.PortalId, orderModel.BillingAddress.EmailAddress, subject, ZnodeConfigManager.SiteConfig.AdminEmail, string.Empty, orderModel.ReceiptHtml, isEnableBcc);
        }

        public override string GetCancelledOrderReceiptForEmail(OrderModel orderModel, out bool isEnableBcc)
        {
            RSZnodeOrderReceipt receipt = new RSZnodeOrderReceipt(orderModel);
            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("CancelledOrderReceipt", (orderModel.PortalId > 0) ? orderModel.PortalId : PortalId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderResendReceiptHtml(emailTemplateMapperModel.Descriptions));
            }
            isEnableBcc = false;
            return string.Empty;
        }

        //to generate shipping status receipt
        //Get Html Resend Receipt For Email.
        public override string GetShippingReceiptForEmail(OrderModel orderModel, out bool isEnableBcc)
        {
            RSZnodeOrderReceipt receipt = new RSZnodeOrderReceipt(orderModel);
            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("ShippingReceipt", (orderModel.PortalId > 0) ? orderModel.PortalId : PortalId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderResendReceiptHtml(emailTemplateMapperModel.Descriptions));
            }
            isEnableBcc = false;
            return string.Empty;
        }

        public string GetSubmittedReceiptForEmail(OrderModel orderModel, out bool isEnableBcc)
        {
            RSZnodeOrderReceipt receipt = new RSZnodeOrderReceipt(orderModel);
            //Method to get Email Template.
            EmailTemplateMapperModel emailTemplateMapperModel = GetEmailTemplateByCode("OrderSubmitted", (orderModel.PortalId > 0) ? orderModel.PortalId : PortalId);
            if (IsNotNull(emailTemplateMapperModel))
            {
                isEnableBcc = emailTemplateMapperModel.IsEnableBcc;
                return EmailTemplateHelper.ReplaceTemplateTokens(receipt.GetOrderResendReceiptHtml(emailTemplateMapperModel.Descriptions));
            }
            isEnableBcc = false;
            return string.Empty;
        }

        #endregion

      
        public override OrderModel CreateOrder(ShoppingCartModel model)
        {
            ZnodeLogging.LogMessage("Execution started.", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Info);
            if (IsNull(model))
                throw new ZnodeException(ErrorCodes.InvalidData, Admin_Resources.ErrorShoppingCartModelNull);

            if (IsAllowedTerritories(model))
                throw new ZnodeException(ErrorCodes.AllowedTerritories, Admin_Resources.AllowedTerritoriesError);

            SubmitOrderModel submitOrderModel = new SubmitOrderModel();

            ParameterModel portalId = new ParameterModel() { Ids = Convert.ToString(model.PortalId) };

            //Get generated unique order number on basis of Stored Procedure.
            model.OrderNumber = GetNextOrdeNumber();
            submitOrderModel.OrderNumber = model.OrderNumber;
           
            ZnodeLogging.LogMessage("Generated order number: ", ZnodeLogging.Components.OMS.ToString(), TraceLevel.Verbose, new { GeneratedOrderNumber = submitOrderModel?.OrderNumber });
            return SaveOrder(model, submitOrderModel);
        }

        //Get generated unique order number on basis of Stored Procedure.
        private string GetNextOrdeNumber()
        {
            string OrderNumber = "";
            try
            {
                IZnodeViewRepository<string> objStoredProc = new ZnodeViewRepository<string>();
                List<string> output = objStoredProc.ExecuteStoredProcedureList("[Znode_NIVI_GetLastOrderNumber]").ToList();
                if(output?.Count>0)
                    OrderNumber =Convert.ToString(output[0]);
                else
                    OrderNumber = GenerateOrderNumber();
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage(ex.Message + "-"+ ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                OrderNumber = GenerateOrderNumber();
                
            }
            return OrderNumber;
        }
        private string GenerateOrderNumber()
        {
            string orderNumber = string.Empty;
            if (!string.IsNullOrEmpty(ZnodeConfigManager.SiteConfig.StoreName))
                orderNumber = ZnodeConfigManager.SiteConfig.StoreName.Trim().Length > 2 ? ZnodeConfigManager.SiteConfig.StoreName.Substring(0, 2) : ZnodeConfigManager.SiteConfig.StoreName.Substring(0, 1);

            DateTime date = DateTime.Now;
                String strDate = date.ToString("yyMMdd-HHmmss-fff");
                orderNumber += $"-{strDate}";
                return orderNumber.ToUpper();
           
        }

        public override OrderModel SaveOrder(ShoppingCartModel model, SubmitOrderModel updateordermodel = null)
        {
            try
            {

                return base.SaveOrder(model, updateordermodel);
            }
            catch (Exception ex)
            {
                ZnodeLogging.LogMessage("SaveOrder" + Convert.ToString(ex) + " StackTrace-" + ex.StackTrace, ZnodeLogging.Components.OMS.ToString(), TraceLevel.Error);
                return new OrderModel();
            }
        }
        //Get order list.
        public override OrdersListModel GetOrderList(NameValueCollection expands, FilterCollection filters, NameValueCollection sorts, NameValueCollection page)
        {
            //Bind the Filter conditions for the authorized portal access.
            BindUserPortalFilter(ref filters);

            //Replace sort key name.
            if (HelperUtility.IsNotNull(sorts))
            {
                ReplaceSortKeys(ref sorts);
            }

            int userId = 0;
            GetUserIdFromFilters(filters, ref userId);

            int fromAdmin = Convert.ToInt32(filters?.Find(x => string.Equals(x.FilterName, Znode.Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin, StringComparison.CurrentCultureIgnoreCase))?.Item3);
            filters?.RemoveAll(x => string.Equals(x.FilterName, Znode.Libraries.ECommerce.Utilities.FilterKeys.IsFromAdmin, StringComparison.CurrentCultureIgnoreCase));
            ReplaceFilterKeys(ref filters);
            PageListModel pageListModel = new PageListModel(filters, sorts, page);

            IList<OrderModel> list = GetOrderList(pageListModel, userId, fromAdmin);
            foreach (OrderModel order in list)
            {
                List<ZnodeOmsOrderLineItem> orderLineItemDetails = _orderLineItemRepository.Table.Where(w => w.OmsOrderDetailsId == order.OmsOrderDetailsId && w.ParentOmsOrderLineItemsId != null).ToList();
                List<OrderLineItemModel> orderLineItemModel = orderLineItemDetails?.ToModel<OrderLineItemModel>().ToList(); ;
                order.OrderLineItems = orderLineItemModel;
                int OrderShipmentId = Convert.ToInt32(orderLineItemDetails.FirstOrDefault()?.OmsOrderShipmentId);
                List<ZnodeOmsOrderShipment> OrderShipment = _orderOrderShipmentRepository.Table.Where(x => x.OmsOrderShipmentId == OrderShipmentId).ToList();
                order.ShippingAddress = OrderShipment.FirstOrDefault().ToModel<AddressModel>();
                ZnodeOmsOrderDetail omsOrderDetail=_orderOrderDetailRepository.Table.Where(w => w.OmsOrderDetailsId == order.OmsOrderDetailsId).FirstOrDefault();
                order.BillingAddress = SetBillingAddress(omsOrderDetail);
               
            }
            OrdersListModel orderListModel = new OrdersListModel() { Orders = list?.ToList() };

            GetCustomerName(userId, orderListModel);

            orderListModel.BindPageListModel(pageListModel);
            return orderListModel;
        }

        private AddressModel SetBillingAddress(ZnodeOmsOrderDetail omsOrderDetail)
        {
            AddressModel addressModel = new AddressModel();
            addressModel.FirstName = omsOrderDetail.BillingFirstName;
            addressModel.LastName = omsOrderDetail.BillingLastName;
            addressModel.EmailAddress = omsOrderDetail.BillingEmailId;

            addressModel.CompanyName = omsOrderDetail.BillingCompanyName;

            addressModel.CityName = omsOrderDetail.BillingCity;

            addressModel.StateCode = omsOrderDetail.BillingStateCode;

            addressModel.CountryCode = omsOrderDetail.CouponCode;

            addressModel.PostalCode = omsOrderDetail.BillingPostalCode;

            addressModel.Address1 = omsOrderDetail.BillingStreet1;

            addressModel.Address2 = omsOrderDetail.BillingStreet2;

            return addressModel;
        }
    }
}
