﻿using System.Collections.Generic;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Sample.Api.Model.Responses
{
    public class CustomPortalDetailListResponse : BaseListResponse
    {
        public List<CustomPortalDetailModel> CustomPortalDetailList { get; set; }
    }
}
